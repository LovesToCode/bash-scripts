#!/bin/bash
#
# MIT License - see LICENSE file
#
# Create a quickstart Maven project from the options.
#
DOMAIN=$1
PROJ=$2
APP=$3
#
mvn archetype:generate \
   -DarchetypeArtifactId=maven-archetype-quickstart \
   -DarchetypeVersion=1.4 \
   -DinteractiveMode=false \
   -DartifactId=$APP \
   -DgroupId=$DOMAIN.$PROJ.$APP
