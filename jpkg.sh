#!/bin/bash
#
# MIT License - see LICENSE file
#
# Create a java app-image
#
# 1 -> main jar file path
# 2 -> name
# 3 -> dest
#
jpackage \
   --type app-image \
   --name $2 \
   --dest $3 \
   --input ./ \
   --main-jar $1
