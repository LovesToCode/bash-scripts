#!/bin/bash
#
# MIT License - see LICENSE file
#
path="$1"
#
/usr/local/bin/chkPath.sh "$path" >/dev/null
if [ $? -eq 1 ]; then
   echo "$path is busy!"
   exit 1
else
   echo "unmounting $path ..."
   sudo /bin/umount "$path" 2>/dev/null
   exit $?
fi
