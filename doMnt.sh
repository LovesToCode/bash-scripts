#!/bin/sh
#
# MIT License - see LICENSE file
#
mount="$1"
RStat=0
#
if grep -qs "$mount" /proc/mounts; then
   echo "$mount is already mounted."
else
   echo "Mounting $mount ..."
   sudo /bin/mount "$mount" 2>/dev/null >/dev/null

   if [ $? -eq 0 ]; then
      echo "Success"
   else
      echo "Mount ERROR!"
      RStat=1
   fi
fi
exit $RStat
