#!/bin/sh
#
# MIT License - see LICENSE file
#
/bin/lsof "$1" 2>/dev/null >/dev/null
if [ $? -eq 0 ]; then
   echo "[$1] is busy"
   exit 1
else
   echo "[$1] is NOT busy."
   exit 0
fi
